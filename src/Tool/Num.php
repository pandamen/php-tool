<?php
declare (strict_types=1);

namespace daxiong\tool;

class Num
{

    /**
     * 数字转表格单位字母
     * @param int $number
     * @return string
     */
    public static function numberToExcelColumn(int $number): string
    {
        $excelColumn = '';
        --$number; // Adjust because Excel columns start at A (1)
        while ($number >= 0) {
            $remainder   = $number % 26; // 0-25
            $excelColumn = chr($remainder + 65) . $excelColumn; // 65 is the ASCII for 'A'
            $number      = (int)($number / 26) - 1; // Convert from 0-based to 1-based
        }
        return $excelColumn;
    }

    /**
     * 使用雪花算法生成订单ID
     * @return string
     * @throws \Exception
     */
    public static function getSnowflakeID(string $prefix = ''): string
    {
        $snowflake   = new \Godruoyi\Snowflake\Snowflake();
        $is_callable = function ($currentTime) {
            $redisSequenceResolver = new \Godruoyi\Snowflake\FileLockResolver();
            return $redisSequenceResolver->sequence($currentTime);
        };
        $id          = $snowflake->setStartTimeStamp(strtotime('2024-10-08') * 1000)->setSequenceResolver($is_callable)->id();
        //32位
        if (PHP_INT_SIZE == 4) {
            $id = abs((int)$id);
        }
        return $prefix . $id;
    }

}