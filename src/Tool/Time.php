<?php
declare (strict_types = 1);

namespace daxiong\tool;

class Time
{
    /**
     * 获取时间范围内日期数组
     *
     * @param int $timestamp1
     * @param int $timestamp2
     * @param string $format
     * @return array
     * @throws \Exception
     */
    public static function getAllDatesBetweenTimestamps(int $timestamp1,int $timestamp2,string $format = 'Y-m-d'): array
    {
        // 创建DateTime对象
        $dateStart = new \DateTime(date('Y-m-d', $timestamp1));
        $dateEnd   = new \DateTime(date('Y-m-d', $timestamp2));

        // 确保$dateStart始终小于或等于$dateEnd
        if ($dateStart > $dateEnd) {
            [$dateStart, $dateEnd] = [$dateEnd, $dateStart];
        }

        // 创建DatePeriod对象
        $interval = \DateInterval::createFromDateString('1 day');
        $period   = new \DatePeriod($dateStart, $interval, $dateEnd->modify('+1 day'));

        // 获取日期数组
        $dates = [];
        foreach ($period as $date) {
            $dates[] = $date->format($format);
        }

        return $dates;
    }
}