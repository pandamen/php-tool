<?php
declare (strict_types = 1);

namespace daxiong\tool;

class Lock
{
    /**
     * 文件锁
     * @param string $filePath
     * @return resource|null
     */
    public static function fileLock(string $filePath)
    {
        // 打开文件
        $fileHandle = fopen($filePath, 'c+');
        if ($fileHandle === false) {
            return null; // 文件打开失败
        }
        // 尝试获取锁
        if (!flock($fileHandle, LOCK_EX | LOCK_NB)) {
            fclose($fileHandle);
            return null; // 获取锁失败
        }
        // 锁定成功
        return $fileHandle;
    }

    /**
     * 释放文件锁
     * @param $fileHandle
     * @param string $lock_file
     * @return bool
     */
    public static function releaseFileLock($fileHandle,string $lock_file = ''): bool
    {
        if (!is_resource($fileHandle)) {
            return false; // 不是有效的文件句柄
        }
        // 释放锁
        if (!flock($fileHandle, LOCK_UN)) {
            return false; // 释放锁失败
        }
        // 关闭文件句柄
        fclose($fileHandle);
        if (\is_file($lock_file)) {
            unlink($lock_file);
        }
        return true;
    }
}