<?php
declare (strict_types = 1);

namespace daxiong\tool;

class Validate
{

    /**
     * 判断当前请求是否通过HTTPS进行。
     *
     * 此函数检查多个头部信息以确保与各种服务器配置和代理兼容。
     * 如果满足以下任何条件之一，则返回true：
     * - 'HTTPS' 环境变量已设置并且不等于 'off'。
     * - 'HTTP_X_FORWARDED_PROTO' 头部被设置为 'https'，表明即使请求通过HTTP代理，
     *   请求也是通过HTTPS发出的。
     * - 'HTTP_FRONT_END_HTTPS' 头部存在并且不等于 'off'，
     *   这些负载均衡器可以用来指示HTTPS请求。
     *
     * @return bool 如果请求是安全的（HTTPS），则返回true；否则返回false。
     */
    public static function is_https(): bool
    {
        if (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
            return true;
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
            return true;
        } elseif (!empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
            return true;
        }
        return false;
    }

    /**
     * 验证输入的手机号码
     * @access  public
     * @param $mobile
     * @return bool
     */
    public static function is_phone($mobile): bool
    {
        // 正则表达式判断手机号
        if (preg_match("/^1\d{10}$/", $mobile)) {
            return true;
        } else {
            return false;
        }
    }
}