<?php
declare (strict_types = 1);
namespace daxiong\tool;

class Arr{

	/**
	 * 对数组排序
	 *
	 * @param array $param 排序前的数组
	 * @return array
	 */
	public static function sort(array &$param): array
    {
		ksort($param);
		reset($param);
		return $param;
	}


}
