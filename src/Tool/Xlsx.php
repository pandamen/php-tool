<?php
declare (strict_types = 1);

namespace daxiong\tool;

class Xlsx
{
    /**
     * 导出xlsx表格
     * @param array $header
     * @param array $data
     * @param string $fileName
     * @return void
     * @throws \Exception
     */
    public static function exportXlsx(array $header = [], array $data = [], string $fileName = '',string $path = ''): void
    {
        if (!extension_loaded('xlswriter')) {
            throw new \Exception('ext-xlswriter 扩展未安装。');
        }
        $path = $path ?: __DIR__ . DIRECTORY_SEPARATOR;
        $config = [
            'path' => $path,
        ];

        $excel  = (new \Vtiful\Kernel\Excel($config))->fileName($fileName . '.xlsx');

        $fileHandle = $excel->getHandle();
        $format2    = new \Vtiful\Kernel\Format($fileHandle);

        $globalStyle = $format2->fontSize(10)
            ->font("Calibri")
            ->align(\Vtiful\Kernel\Format::FORMAT_ALIGN_CENTER, \Vtiful\Kernel\Format::FORMAT_ALIGN_VERTICAL_CENTER)
            ->border(\Vtiful\Kernel\Format::BORDER_THIN)
            ->toResource();

        $excel->setColumn('A:' . Num::numberToExcelColumn(count($header)), 16, $globalStyle);

        $filePath = $excel->header($header)->data($data)->output();

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        header('Content-Length: ' . filesize($filePath));
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Cache-Control: max-age=0');
        header('Pragma: public');

        ob_clean();
        flush();

        if (copy($filePath, 'php://output') === false) {
            // Throw exception
        }
        @unlink($filePath);
        $excel->close();
    }
}