<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'daxiong/tool',
        'dev' => true,
    ),
    'versions' => array(
        'daxiong/tool' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'godruoyi/php-snowflake' => array(
            'pretty_version' => '2.2.4',
            'version' => '2.2.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../godruoyi/php-snowflake',
            'aliases' => array(),
            'reference' => '833ce5a542034c847ae9d74c4d89c9592cf77a4e',
            'dev_requirement' => false,
        ),
    ),
);
